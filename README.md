# Journal Abbreviation List

abbreviations.txt is a tab-delimited journal abbreviations list in Endnote file
term style. Designed to be used with the script
[bibtexformat](http://www.bulheller.com/bibtexformat.html). Typical usage is:

```
$ bibtexformat <input bibtex file.bib> -o <output>.bib -s -format -wrap 80 -abb -sort
```
## Version 
* Version 0.1

## Contact
#### Developer/Company
* Name: Shane Gordon
* e-mail: se2gordon@students.latrobe.edu.au
